# **********************************************************************
# $Id: collisions_run.config
# **********************************************************************

#######################
# HLTminbias
#######################

#######################
# References
#######################

reference HLTminbias_Ref {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/
  file = data12_8TeV.00200805.physics_MinBias.merge.HIST.f432_m1108._0001.1_sedwardg_120409.root
  path = run_200805
  name = same_name
}

#######################
# Output
#######################


output top_level {
   output HLT {
      #### Run 3 output folders -- example 
      output Run3 {
         output TRMBI {
            output SHIFTER {
              output SPTRK {
              }
              output MBTS {
              }
            }
            output EXPERT {
              output SPTRK {
              }
              output MBTS {                
              }
            }
         }
      }  # end output Run3


      #### Run 2 output folders
      output Run2 {
         output TRMBI {
            output SHIFTER {
            }
            output EXPERT {
               output General {
               }
               output MBTS {
               }
               output HMT {
	                output mb_sp1500_hmtperf_L1TE10 {
	                }
	                output mb_sp700_hmtperf_L1TE5 {
	                }
	                output mb_sp2100_hmtperf_L1TE10 {
	                }
               }
               output IDMinbias {
               }
               output UPC {
    	          output mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 {
	            }
	           }
            }
         } ## end output TRMBI
      } ## end output Run2
   } # end output HLT
} # end output top_level


#######################
# Histogram Assessments
#######################

#### Run 3 histograms -- example 
dir HLT {
  algorithm = HLTminbias_GatherData
  dir MinBiasMon {
    hist whichTrigger {
      output = HLT/Run3/TRMBI/SHIFTER
      description 	= "Filled when trigger are active, there is no 'normal'"

      display = LogY
    }
    dir Tracking {
      dir HLT_mb_sptrk_L1RD0_FILLED {
        hist efficiencyLowMult {
          output = HLT/Run3/TRMBI/SHIFTER
          description 	= Normal: Quick rise to almost 100% for 1,2 tracks, no eff. drop for higher multiplicities
        }
        hist nTrkOfflineLowMult {
          output = HLT/Run3/TRMBI/SHIFTER
          description 	= Normal: Quick rise to almost 100% for 1,2 tracks, no eff. drop for higher multiplicities
        }
        hist nTrkOnlineLowMult {
          output = HLT/Run3/TRMBI/SHIFTER
          description 	= Normal: Sharp rise at ntrk ==1, (literally has to be a step function)
        }

      }
      dir HLT_mb_sp.* {
        hist nTrkRatio {
          output = HLT/Run3/TRMBI/EXPERT/SPTRK
          description 	= Normal: narrow peak slightly below 1 (0.9), if much below it means online tracking has to many fakes
        }
      }
      dir HLT_.*hmt.* {
        hist efficiencyAnyMult {
          output = HLT/Run3/TRMBI/SHIFTER/SPTRK
          description 	= Normal: Typical trigger turn-on, should be reaching max around the threshold specified in the trigger name
        }
        hist nTrkOnline {
          output = HLT/Run3/TRMBI/EXPERT/SPTRK
          description 	= Normal: exponent like distribution with a hard cut from the left side
        }
        hist nTrkOffline {
          output = HLT/Run3/TRMBI/EXPERT/SPTRK
          description 	= Normal: exponent like distribution with a soft cut from the left side
        }
      }
    }
  } # end dir MinBiasMon
 } # end dir HLT


#### Run 2 histograms
dir HLT {
  algorithm = HLTminbias_GatherData
  dir MinBiasMon {
    #histos for shifter dir
    hist TriggerEntries@Shifter {
      output = HLT/Run2/TRMBI/SHIFTER
    }
    hist TriggerEfficiencies@Shifter {
      output = HLT/Run2/TRMBI/SHIFTER
    }
    
    dir MBTS {
      dir mb_perf_L1MBTS_1_1 {
        hist Occupancy@Shifter {
          output = HLT/Run2/TRMBI/SHIFTER
        }
        hist MbtsCorr_N_N@Shifter {
	        output = HLT/Run2/TRMBI/SHIFTER
	        display = LogZ
      	}
	      hist Time@Shifter {
          output = HLT/Run2/TRMBI/SHIFTER
        }
      }
    }

    dir IDMinbias {
      dir mb_sptrk {
        hist PixTot@Shifter {
          output = HLT/Run2/TRMBI/SHIFTER
        }
        hist SctTot@Shifter {
          output = HLT/Run2/TRMBI/SHIFTER 
        }
      }
    }

    #histos for general   
    hist TriggerPurities@General {
      output = HLT/Run2/TRMBI/EXPERT/General
    } 
    hist TriggerEntries@General {
      output = HLT/Run2/TRMBI/EXPERT/General
    }
    
    hist TriggerEfficiencies@General {
      output = HLT/Run2/TRMBI/EXPERT/General
    }

    dir Purities&Efficiencies {
      dir mb_sptrk {
	      output = HLT/Run2/TRMBI/EXPERT/General
	      hist NumGoodOfflineTracks@General {
	        display = LogY
	      }
	      hist GoodOfflineTracksPt@General {
	        display = LogY
	      }
	      hist Purity@General {
      	}
      }
    } 
    
    
    #now for HLT/Run2/TRMBI/EXPERT/MBTS dir
    dir MBTS {
      dir mb_perf_L1MBTS_1_1 {
	      output = HLT/Run2/TRMBI/EXPERT/MBTS
        hist Occupancy@MBTS {
        }
        hist OccupancyOnline@MBTS { 
        }
        hist Time@MBTS {
        }
        hist TimeOnline@MBTS {
        }
        hist MbtsEnergyErrors@MBTS {
        }
        hist MbtsTimeErrors@MBTS {
        }     
        hist MbtsCorr_N_N@MBTS {
	        display = LogZ
        }
      }
    }
    
    # now for IDMinbias
    dir IDMinbias {
      dir mb_sptrk {
	      output = HLT/Run2/TRMBI/EXPERT/IDMinbias
	      hist PixBarr_SP@IDMinbias {
      	}
	      hist PixECA_SP@IDMinbias {
	      }
	      hist PixECC_SP@IDMinbias {
	      }
	      hist SctBarr_SP@IDMinbias {
	      }
	      hist SctECA_SP@IDMinbias {
	      }
	      hist SctECC_SP@IDMinbias {
	      }
	      hist PixTot@IDMinbias {
	      }
	      hist SctTot@IDMinbias {
	      }
	      hist MinbiasTracks@IDMinbias {
	      }
      }
    }

    dir HMT {
      dir mb_sp1500_hmtperf_L1TE10 {
	      output = HLT/Run2/TRMBI/EXPERT/HMT/mb_sp1500_hmtperf_L1TE10
        hist NumSpacePoints@HMT1 {
        }
        hist NumVertices@HMT1 {
        }
        hist NumTracksAtVertex@HMT1 {
        }
        hist NumHitsAtVertex@HMT1 {
        }
      }

      dir mb_sp700_hmtperf_L1TE5 {
	      output = HLT/Run2/TRMBI/EXPERT/HMT/mb_sp700_hmtperf_L1TE5
        hist NumSpacePoints@HMT2 {
	      }
	      hist NumVertices@HMT2 {
	      }
	      hist NumTracksAtVertex@HMT2 {
	      }
      	hist NumHitsAtVertex@HMT2 {
	      }
      }

      dir mb_sp2100_hmtperf_L1TE10 {
	      output = HLT/Run2/TRMBI/EXPERT/HMT/mb_sp2100_hmtperf_L1TE10
	      hist NumSpacePoints@HMT4 {
	      }
	      hist NumVertices@HMT4 {
	      }
	      hist NumTracksAtVertex@HMT4 {
	      }
	      hist NumHitsAtVertex@HMT4 {
	      }
      }
    }
    #now for ALFA
    dir Purities&Efficiencies {
      dir mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 {
	      output = HLT/Run2/TRMBI/EXPERT/UPC/mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50
	      hist Purity@ALFA1 {
	      }
	      hist NumGoodOfflineTracks@ALFA1 {
	        display = LogY
	      }
	      hist GoodOfflineTracksPt@ALFA1 {
	        display = LogY
	      }
      }
    }
    dir MBTS {
      dir mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50 {
	      output = HLT/Run2/TRMBI/EXPERT/UPC/mb_sptrk_vetombts2in_L1ZDC_A_C_VTE50
	      hist OccupancyOnline@ALFA1 { 
	      }
	      hist TimeOnline@ALFA1 {
	      }    
	      hist MbtsCorr_N_N@ALFA1 {
	        display = LogZ
	      }
      }
    }
  }
}

##############
# Algorithms
##############

algorithm HLTminbias_Histogram_Not_Empty&GatherData {
  libname = libdqm_algorithms.so
  name = HLT_Histogram_Not_Empty&GatherData
  reference = HLTminbias_Ref
} 

algorithm HLTminbias_GatherData {
  libname = libdqm_algorithms.so
  name = GatherData
}


###############
# Thresholds
###############
