# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Logging import logging
log = logging.getLogger(__name__)

from ..Base.TopoAlgos import EMMultiplicityAlgo, TauMultiplicityAlgo, JetMultiplicityAlgo, XEMultiplicityAlgo

class TopoAlgoDefMultiplicity(object):
    """
    Defines the TopoAlgorithms that calculate multiplicities for L1Calo thresholds
    The thresholds have to be explicitly defined here.
    """
    @staticmethod
    def registerTopoAlgos(tm):
        currentAlgoId = 0

        emThresholds = [
            'eEM3', 'eEM7', 'eEM8', 'eEM8L', 'eEM10', 'eEM10L',  'eEM15', 'eEM15L', 'eEM15M', 'eEM18M', 
            'eEM20', 'eEM20L', 'eEM20M', 'eEM22', 'eEM22M', 'eEM22T', 
        ]

        for em in emThresholds:
            alg = EMMultiplicityAlgo( name = em,
                                      algoId = currentAlgoId,
                                      threshold = em,
                                      nbits = 3)
            tm.registerTopoAlgo(alg)
                
        # eTAU 3 bits (4 Thresholds)
        tauThresholds_3bits = [ "eTAU8", "eTAU12", "eTAU12M", "eTAU20" ]
        # eTAU 2 bits (max 12 Thresholds, 16 in production)
        tauThresholds_2bits = [ "eTAU20M", "eTAU25", "eTAU25M", "eTAU30H", "eTAU40", "eTAU60", "eTAU100" ]

        for tau in tauThresholds_3bits:
            alg = TauMultiplicityAlgo( name = tau,
                                       algoId = currentAlgoId,
                                       threshold = tau,
                                       nbits = 3)
            tm.registerTopoAlgo(alg)
        for tau in tauThresholds_2bits:
            alg = TauMultiplicityAlgo( name = tau,
                                       algoId = currentAlgoId,
                                       threshold = tau,
                                       nbits = 2)
            tm.registerTopoAlgo(alg)

        jJThresholds = [ "jJ12", "jJ12p0ETA25", "jJ15", "jJ15p0ETA25", "jJ20", "jJ25", "jJ25p0ETA23", "jJ30", 
                         "jJ35p0ETA23", "jJ40p0ETA25", "jJ40", "jJ50", "jJ85", "jJ100", "jJ15p31ETA49", "jJ20p31ETA49", "jJ75p31ETA49" ]

        for jJet in jJThresholds:
            alg = JetMultiplicityAlgo( name = jJet,
                                       algoId = currentAlgoId,
                                       threshold = jJet,
                                       nbits = 3)
            tm.registerTopoAlgo(alg)


        gXEThresholds = [ "gXERHO20", "gXERHO30", "gXERHO35", "gXERHO40", "gXERHO45", "gXERHO50", "gXEPUFIT20", "gXEPUFIT50", "gXE50" ]

        for gXE in gXEThresholds:
            alg = XEMultiplicityAlgo( name = gXE,
                                      algoId = currentAlgoId,
                                      threshold = gXE,
                                      nbits = 1)
            tm.registerTopoAlgo(alg)


        jXEThresholds = [ "jXE50" ]

        for jXE in jXEThresholds:
            alg = XEMultiplicityAlgo( name = jXE,
                                      algoId = currentAlgoId,
                                      threshold = jXE,
                                      nbits = 1)
            tm.registerTopoAlgo(alg)

