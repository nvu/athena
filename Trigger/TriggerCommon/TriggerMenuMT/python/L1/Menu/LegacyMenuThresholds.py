# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

"""
This list defines the set of legacy L1Calo thresholds to be used at the beginning of Run3 (ATR-23241)

"""

legacyThresholds = [

    "EM3", "EM7", "EM8VH", "EM10VH", "EM12", "EM15", "EM15VH", "EM15VHI", "EM18VHI", "EM20VH", "EM20VHI", "EM22VH", "EM22VHI", "EM24VHI",

    "HA8", "HA12IM", "HA20IM", "HA25IM", "HA40", "HA60", "HA100",

    "J12", "J12p0ETA25", "J15", "J15p0ETA25", "J15p31ETA49", "J20", "J20p31ETA49", "J25", "J25p0ETA23", "J30", "J30p31ETA49", "J35p0ETA23",
    "J40", "J40p0ETA25",  "J45p0ETA21", "J50", "J50p31ETA49", "J75", "J75p31ETA49", "J85", "J100", "J120", "J400", 

    "TE5", "TE10", "TE20", "TE50",

    "XE30", "XE35", "XE40", "XE45", "XE50", "XE55", "XE60", "XE300",

    # detector thresholds
    "NIMTGC", "NIMTRT", "MBTS_A", "MBTS_C", "CAL1", "CAL2", "BCM_AtoC", "BCM_CtoA",

]
